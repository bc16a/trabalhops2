import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class DiretorioMusicas extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;


  constructor(data?: Partial<DiretorioMusicas>) {
    super(data);
  }
}

export interface DiretorioMusicasRelations {
  // describe navigational properties here
}

export type DiretorioMusicasWithRelations = DiretorioMusicas & DiretorioMusicasRelations;
