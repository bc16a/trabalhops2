import {DefaultCrudRepository} from '@loopback/repository';
import {PlaylistMusica, PlaylistMusicaRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class PlaylistMusicaRepository extends DefaultCrudRepository<
  PlaylistMusica,
  typeof PlaylistMusica.prototype.id,
  PlaylistMusicaRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(PlaylistMusica, dataSource);
  }
}
