import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class Album extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  nome: string;

  @property({
    type: 'string',
    required: true,
  })
  nomebanda: string;

  @property({
    type: 'string',
    required: true,
  })
  estilo: string;


  constructor(data?: Partial<Album>) {
    super(data);
  }
}

export interface AlbumRelations {
  // describe navigational properties here
}

export type AlbumWithRelations = Album & AlbumRelations;
