import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {Playlist, PlaylistRelations, PlaylistMusica} from '../models';
import {DbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {PlaylistMusicaRepository} from './playlist-musica.repository';

export class PlaylistRepository extends DefaultCrudRepository<
  Playlist,
  typeof Playlist.prototype.id,
  PlaylistRelations
> {

  public readonly playlistMusicas: HasManyRepositoryFactory<PlaylistMusica, typeof Playlist.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('PlaylistMusicaRepository') protected playlistMusicaRepositoryGetter: Getter<PlaylistMusicaRepository>,
  ) {
    super(Playlist, dataSource);
    this.playlistMusicas = this.createHasManyRepositoryFactoryFor('playlistMusicas', playlistMusicaRepositoryGetter,);
  }
}
