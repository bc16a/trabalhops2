import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class PlaylistMusica extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'number',
  })
  playlistId?: number;

  @property({
    type: 'number',
  })
  musicaId?: number;

  constructor(data?: Partial<PlaylistMusica>) {
    super(data);
  }
}

export interface PlaylistMusicaRelations {
  // describe navigational properties here
}

export type PlaylistMusicaWithRelations = PlaylistMusica & PlaylistMusicaRelations;
