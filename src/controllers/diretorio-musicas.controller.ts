import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {DiretorioMusicas} from '../models';
import {DiretorioMusicasRepository} from '../repositories';

export class DiretorioMusicasController {
  constructor(
    @repository(DiretorioMusicasRepository)
    public diretorioMusicasRepository : DiretorioMusicasRepository,
  ) {}

  @post('/diretorio-musicas', {
    responses: {
      '200': {
        description: 'DiretorioMusicas model instance',
        content: {'application/json': {schema: {'x-ts-type': DiretorioMusicas}}},
      },
    },
  })
  async create(@requestBody() diretorioMusicas: DiretorioMusicas): Promise<DiretorioMusicas> {
    return await this.diretorioMusicasRepository.create(diretorioMusicas);
  }

  @get('/diretorio-musicas/count', {
    responses: {
      '200': {
        description: 'DiretorioMusicas model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(DiretorioMusicas)) where?: Where<DiretorioMusicas>,
  ): Promise<Count> {
    return await this.diretorioMusicasRepository.count(where);
  }

  @get('/diretorio-musicas', {
    responses: {
      '200': {
        description: 'Array of DiretorioMusicas model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: {'x-ts-type': DiretorioMusicas}},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(DiretorioMusicas)) filter?: Filter<DiretorioMusicas>,
  ): Promise<DiretorioMusicas[]> {
    return await this.diretorioMusicasRepository.find(filter);
  }

  @patch('/diretorio-musicas', {
    responses: {
      '200': {
        description: 'DiretorioMusicas PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody() diretorioMusicas: DiretorioMusicas,
    @param.query.object('where', getWhereSchemaFor(DiretorioMusicas)) where?: Where<DiretorioMusicas>,
  ): Promise<Count> {
    return await this.diretorioMusicasRepository.updateAll(diretorioMusicas, where);
  }

  @get('/diretorio-musicas/{id}', {
    responses: {
      '200': {
        description: 'DiretorioMusicas model instance',
        content: {'application/json': {schema: {'x-ts-type': DiretorioMusicas}}},
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<DiretorioMusicas> {
    return await this.diretorioMusicasRepository.findById(id);
  }

  @patch('/diretorio-musicas/{id}', {
    responses: {
      '204': {
        description: 'DiretorioMusicas PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody() diretorioMusicas: DiretorioMusicas,
  ): Promise<void> {
    await this.diretorioMusicasRepository.updateById(id, diretorioMusicas);
  }

  @put('/diretorio-musicas/{id}', {
    responses: {
      '204': {
        description: 'DiretorioMusicas PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() diretorioMusicas: DiretorioMusicas,
  ): Promise<void> {
    await this.diretorioMusicasRepository.replaceById(id, diretorioMusicas);
  }

  @del('/diretorio-musicas/{id}', {
    responses: {
      '204': {
        description: 'DiretorioMusicas DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.diretorioMusicasRepository.deleteById(id);
  }
}
