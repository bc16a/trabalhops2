import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {PlaylistMusica} from '../models';
import {PlaylistMusicaRepository} from '../repositories';

export class PlaylistMusicaController {
  constructor(
    @repository(PlaylistMusicaRepository)
    public playlistMusicaRepository : PlaylistMusicaRepository,
  ) {}

  @post('/playlist-musicas', {
    responses: {
      '200': {
        description: 'PlaylistMusica model instance',
        content: {'application/json': {schema: {'x-ts-type': PlaylistMusica}}},
      },
    },
  })
  async create(@requestBody() playlistMusica: PlaylistMusica): Promise<PlaylistMusica> {
    return await this.playlistMusicaRepository.create(playlistMusica);
  }

  @get('/playlist-musicas/count', {
    responses: {
      '200': {
        description: 'PlaylistMusica model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(PlaylistMusica)) where?: Where<PlaylistMusica>,
  ): Promise<Count> {
    return await this.playlistMusicaRepository.count(where);
  }

  @get('/playlist-musicas', {
    responses: {
      '200': {
        description: 'Array of PlaylistMusica model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: {'x-ts-type': PlaylistMusica}},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(PlaylistMusica)) filter?: Filter<PlaylistMusica>,
  ): Promise<PlaylistMusica[]> {
    return await this.playlistMusicaRepository.find(filter);
  }

  @patch('/playlist-musicas', {
    responses: {
      '200': {
        description: 'PlaylistMusica PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody() playlistMusica: PlaylistMusica,
    @param.query.object('where', getWhereSchemaFor(PlaylistMusica)) where?: Where<PlaylistMusica>,
  ): Promise<Count> {
    return await this.playlistMusicaRepository.updateAll(playlistMusica, where);
  }

  @get('/playlist-musicas/{id}', {
    responses: {
      '200': {
        description: 'PlaylistMusica model instance',
        content: {'application/json': {schema: {'x-ts-type': PlaylistMusica}}},
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<PlaylistMusica> {
    return await this.playlistMusicaRepository.findById(id);
  }

  @patch('/playlist-musicas/{id}', {
    responses: {
      '204': {
        description: 'PlaylistMusica PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody() playlistMusica: PlaylistMusica,
  ): Promise<void> {
    await this.playlistMusicaRepository.updateById(id, playlistMusica);
  }

  @put('/playlist-musicas/{id}', {
    responses: {
      '204': {
        description: 'PlaylistMusica PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() playlistMusica: PlaylistMusica,
  ): Promise<void> {
    await this.playlistMusicaRepository.replaceById(id, playlistMusica);
  }

  @del('/playlist-musicas/{id}', {
    responses: {
      '204': {
        description: 'PlaylistMusica DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.playlistMusicaRepository.deleteById(id);
  }
}
