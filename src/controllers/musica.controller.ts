import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Musica} from '../models';
import {MusicaRepository} from '../repositories';

export class MusicaController {
  constructor(
    @repository(MusicaRepository)
    public musicaRepository : MusicaRepository,
  ) {}

  @post('/musicas', {
    responses: {
      '200': {
        description: 'Musica model instance',
        content: {'application/json': {schema: {'x-ts-type': Musica}}},
      },
    },
  })
  async create(@requestBody() musica: Musica): Promise<Musica> {
    return await this.musicaRepository.create(musica);
  }

  @get('/musicas/count', {
    responses: {
      '200': {
        description: 'Musica model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Musica)) where?: Where<Musica>,
  ): Promise<Count> {
    return await this.musicaRepository.count(where);
  }

  @get('/musicas', {
    responses: {
      '200': {
        description: 'Array of Musica model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: {'x-ts-type': Musica}},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Musica)) filter?: Filter<Musica>,
  ): Promise<Musica[]> {
    return await this.musicaRepository.find(filter);
  }

  @patch('/musicas', {
    responses: {
      '200': {
        description: 'Musica PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody() musica: Musica,
    @param.query.object('where', getWhereSchemaFor(Musica)) where?: Where<Musica>,
  ): Promise<Count> {
    return await this.musicaRepository.updateAll(musica, where);
  }

  @get('/musicas/{id}', {
    responses: {
      '200': {
        description: 'Musica model instance',
        content: {'application/json': {schema: {'x-ts-type': Musica}}},
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Musica> {
    return await this.musicaRepository.findById(id);
  }

  @patch('/musicas/{id}', {
    responses: {
      '204': {
        description: 'Musica PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody() musica: Musica,
  ): Promise<void> {
    await this.musicaRepository.updateById(id, musica);
  }

  @put('/musicas/{id}', {
    responses: {
      '204': {
        description: 'Musica PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() musica: Musica,
  ): Promise<void> {
    await this.musicaRepository.replaceById(id, musica);
  }

  @del('/musicas/{id}', {
    responses: {
      '204': {
        description: 'Musica DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.musicaRepository.deleteById(id);
  }
}
