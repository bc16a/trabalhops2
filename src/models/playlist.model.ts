import {Entity, model, property, hasMany} from '@loopback/repository';
import {PlaylistMusica} from './playlist-musica.model';

@model({settings: {}})
export class Playlist extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  nome: string;

  @property({
    type: 'string',
    required: true,
  })
  estilop: string;

  @property({
    type: 'string',
    required: true,
  })
  estilos: string;

  @hasMany(() => PlaylistMusica)
  playlistMusicas: PlaylistMusica[];

  constructor(data?: Partial<Playlist>) {
    super(data);
  }
}

export interface PlaylistRelations {
  // describe navigational properties here
}

export type PlaylistWithRelations = Playlist & PlaylistRelations;
