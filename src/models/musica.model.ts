import {Entity, model, property, hasMany} from '@loopback/repository';
import {PlaylistMusica} from './playlist-musica.model';

@model({settings: {}})
export class Musica extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  estilo: string;

  @property({
    type: 'string',
    required: true,
  })
  nome: string;

  @property({
    type: 'number',
    required: true,
  })
  duracao: number;

  @hasMany(() => PlaylistMusica)
  playlistMusicas: PlaylistMusica[];

  constructor(data?: Partial<Musica>) {
    super(data);
  }
}

export interface MusicaRelations {
  // describe navigational properties here
}

export type MusicaWithRelations = Musica & MusicaRelations;
