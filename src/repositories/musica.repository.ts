import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {Musica, MusicaRelations, PlaylistMusica} from '../models';
import {DbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {PlaylistMusicaRepository} from './playlist-musica.repository';

export class MusicaRepository extends DefaultCrudRepository<
  Musica,
  typeof Musica.prototype.id,
  MusicaRelations
> {

  public readonly playlistMusicas: HasManyRepositoryFactory<PlaylistMusica, typeof Musica.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('PlaylistMusicaRepository') protected playlistMusicaRepositoryGetter: Getter<PlaylistMusicaRepository>,
  ) {
    super(Musica, dataSource);
    this.playlistMusicas = this.createHasManyRepositoryFactoryFor('playlistMusicas', playlistMusicaRepositoryGetter,);
  }
}
