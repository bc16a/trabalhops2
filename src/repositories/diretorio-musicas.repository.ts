import {DefaultCrudRepository} from '@loopback/repository';
import {DiretorioMusicas, DiretorioMusicasRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DiretorioMusicasRepository extends DefaultCrudRepository<
  DiretorioMusicas,
  typeof DiretorioMusicas.prototype.id,
  DiretorioMusicasRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(DiretorioMusicas, dataSource);
  }
}
