import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Playlist,
  PlaylistMusica,
} from '../models';
import {PlaylistRepository} from '../repositories';

export class PlaylistPlaylistMusicaController {
  constructor(
    @repository(PlaylistRepository) protected playlistRepository: PlaylistRepository,
  ) { }

  @get('/playlists/{id}/playlist-musicas', {
    responses: {
      '200': {
        description: 'Array of PlaylistMusica\'s belonging to Playlist',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': PlaylistMusica } },
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<PlaylistMusica>,
  ): Promise<PlaylistMusica[]> {
    return await this.playlistRepository.playlistMusicas(id).find(filter);
  }

  @post('/playlists/{id}/playlist-musicas', {
    responses: {
      '200': {
        description: 'Playlist model instance',
        content: { 'application/json': { schema: { 'x-ts-type': PlaylistMusica } } },
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Playlist.prototype.id,
    @requestBody() playlistMusica: PlaylistMusica,
  ): Promise<PlaylistMusica> {
    return await this.playlistRepository.playlistMusicas(id).create(playlistMusica);
  }

  @patch('/playlists/{id}/playlist-musicas', {
    responses: {
      '200': {
        description: 'Playlist.PlaylistMusica PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody() playlistMusica: Partial<PlaylistMusica>,
    @param.query.object('where', getWhereSchemaFor(PlaylistMusica)) where?: Where<PlaylistMusica>,
  ): Promise<Count> {
    return await this.playlistRepository.playlistMusicas(id).patch(playlistMusica, where);
  }

  @del('/playlists/{id}/playlist-musicas', {
    responses: {
      '200': {
        description: 'Playlist.PlaylistMusica DELETE success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(PlaylistMusica)) where?: Where<PlaylistMusica>,
  ): Promise<Count> {
    return await this.playlistRepository.playlistMusicas(id).delete(where);
  }
}
