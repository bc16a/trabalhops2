import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Musica,
  PlaylistMusica,
} from '../models';
import {MusicaRepository} from '../repositories';

export class MusicaPlaylistMusicaController {
  constructor(
    @repository(MusicaRepository) protected musicaRepository: MusicaRepository,
  ) { }

  @get('/musicas/{id}/playlist-musicas', {
    responses: {
      '200': {
        description: 'Array of PlaylistMusica\'s belonging to Musica',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': PlaylistMusica } },
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<PlaylistMusica>,
  ): Promise<PlaylistMusica[]> {
    return await this.musicaRepository.playlistMusicas(id).find(filter);
  }

  @post('/musicas/{id}/playlist-musicas', {
    responses: {
      '200': {
        description: 'Musica model instance',
        content: { 'application/json': { schema: { 'x-ts-type': PlaylistMusica } } },
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Musica.prototype.id,
    @requestBody() playlistMusica: PlaylistMusica,
  ): Promise<PlaylistMusica> {
    return await this.musicaRepository.playlistMusicas(id).create(playlistMusica);
  }

  @patch('/musicas/{id}/playlist-musicas', {
    responses: {
      '200': {
        description: 'Musica.PlaylistMusica PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody() playlistMusica: Partial<PlaylistMusica>,
    @param.query.object('where', getWhereSchemaFor(PlaylistMusica)) where?: Where<PlaylistMusica>,
  ): Promise<Count> {
    return await this.musicaRepository.playlistMusicas(id).patch(playlistMusica, where);
  }

  @del('/musicas/{id}/playlist-musicas', {
    responses: {
      '200': {
        description: 'Musica.PlaylistMusica DELETE success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(PlaylistMusica)) where?: Where<PlaylistMusica>,
  ): Promise<Count> {
    return await this.musicaRepository.playlistMusicas(id).delete(where);
  }
}
